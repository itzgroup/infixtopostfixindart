import 'dart:io';

List<String> listOperators = [];
List<String> listPostfix = [];
List<String> listInfix = [];
List<int> listValue = [];

void main() {
  //iput
  print("input and then space it! ");
  print("input Proposition : ");
  var word = stdin.readLineSync()!;

  infix(word);
  print("InFix : ");
  print(listInfix);
  print("Postfix : ");
  processConvert(listInfix);
  print(listPostfix);
  evaluatorPostfix(listPostfix);
  print("Evaluate Postfix : ");
  print(listValue[0]);
}

//infix
List<String> infix(var x) {
  listInfix = x.split(" ");
  return listInfix;
}

//convert Infix to Postfix
void processConvert(List<String> tokens) {
  for (var i = 0; i < listInfix.length; i++) {
    if (tokens[i] == "+" ||
        tokens[i] == "-" ||
        tokens[i] == "*" ||
        tokens[i] == "/" ||
        tokens[i] == "^") {
      //input + , -
      while (listOperators.isNotEmpty &&
          listOperators.last != "(" &&
          (tokens[i] == "+" ||
              tokens[i] == "-" && listOperators.last == "*" ||
              listOperators.last == "/" ||
              listOperators.last == "^")) {
        listPostfix.add(listOperators.last);
        listOperators.removeLast();
      }
      //input + , /
      while (listOperators.isNotEmpty &&
          listOperators.last != "(" &&
          (tokens[i] == "*" ||
              tokens[i] == "/" && listOperators.last == "*" ||
              listOperators.last == "/" ||
              listOperators.last == "^")) {
        listPostfix.add(listOperators.last);
        listOperators.removeLast();
      }
      listOperators.add(tokens[i]);
    } else if (tokens[i] == "(") {
      listOperators.add(tokens[i]);
    } else if (tokens[i] == ")") {
      while (listOperators.last != "(") {
        listPostfix.add(listOperators.last);
        listOperators.removeLast();
      }
      listOperators.removeLast();
    } else {
      listPostfix.add(tokens[i]);
    }
  }
  while (listOperators.isNotEmpty) {
    listPostfix.add(listOperators.last);
    listOperators.removeLast();
  }
}

void evaluatorPostfix(List<String> tokens) {
  for (var i = 0; i < listPostfix.length; i++) {
    if (tokens[i] == "+" ||
        tokens[i] == "-" ||
        tokens[i] == "*" ||
        tokens[i] == "/" ||
        tokens[i] == "^") {
      int right = listValue.removeLast();
      int left = listValue.removeLast();
      int sum = 1;
      switch (tokens[i]) {
        case "+":
          listValue.add(left + right);
          break;
        case "-":
          listValue.add(left - right);
          break;
        case "*":
          listValue.add(left * right);
          break;
        case "/":
          listValue.add(left ~/ right);
          break;
        case "^":
          for (int j = 0; j < right; j++) {
            sum *= left;
          }
          listValue.add(sum);
          break;
      }
    } else {
      listValue.add(int.parse(tokens[i]));
    }
  }
}
